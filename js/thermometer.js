(function ($) {
  Drupal.behaviors.thermometer = {
    attach: function (context, settings) {
      // Process all the thermometers
      var thermometers = settings.rgraph_thermometer;
      for (var i = 0; i < thermometers.length; ++i) {
        thermometer_config = thermometers[i];

        // Extract the thermometer values
        var min = thermometer_config.min;
        var max = thermometer_config.max;
        var current = thermometer_config.current;
        var html_id = thermometer_config.html_id;
        var gradient_start = thermometer_config.gradient_start;
        var gradient_middle = thermometer_config.gradient_middle;
        var gradient_end = thermometer_config.gradient_end;

        // Create the themometer
        var thermometer = new RGraph.Thermometer(html_id, min, max, current);
        var right_gutter = calculate_right_gutter(thermometer);

        // Size the thermometer
        var width = $('#' + html_id).attr('width'); - right_gutter / 2;
        var height = $('#' + html_id).attr('height');

        // Construct the gradient
        var grad = thermometer.context.createLinearGradient(0, 0, width, 0);
        grad.addColorStop(0, gradient_start);
        grad.addColorStop(0.5, gradient_middle);
        grad.addColorStop(1, gradient_end);

        // Render the thermometer
        thermometer.Set('chart.colors', [grad]);
        thermometer.Set('chart.scale.visible', true);
        thermometer.Set('chart.gutter.right', right_gutter);
        thermometer.Draw();
      }
    }
  };

  /**
   * Calculates the right gutter for a thermometer
   *
   * @param thermometer
   *  The RGraph.thermometer object
   *
   * @return The optimal width for the right gutter
   */
  function calculate_right_gutter(thermometer) {
    var longest_string = thermometer.max.toString();
    var separator_count = (longest_string.length - 1) / 3;
    for (var i = 0; i < separator_count; ++i) {
      longest_string += ',';
    }
    var text_metrics = thermometer.context.measureText(longest_string);
    return text_metrics.width;
  }
})(jQuery);
