Last updated: 2012-10-20

Introduction
=====================================================
The RGraph Thermometer module allows authorized users to create thermometers for fundraising, goal tracking, etc. The thermometer is rendered using the RGraph Javascript libary.

This module was created for the Silverlines Philanthropy Corporation by Christopher Millward.

Requirements
=====================================================
As this module uses the RGraph Javascript library, HTML5 Canvas support is required.

Installation
=====================================================
Installation is relatively straightforward:

1. Visit the RGraph website at http://www.rgraph.net/ and download the latest version of the RGraph library.  This module has not yet been tested with the Oct. 2012 Beta release, but it should fine with anything later than the July 2012 (Stable) release.
2. Extract the library as-is into the appropriate libraries folder (sites/all/libraries or sites/example.com/libraries).
3. Install this module.

If all goes well, the Status Reports page should indicate that the RGraph library is installed.

Usage
=====================================================
Thermometers are a content type and thus can be created / published / administered like any other content type in Drupal.

To add a new thermometer:
1. Go to node/add/rgraph-thermometer
2. Fill in the Minimum value, Maximum value, and Current value fields, then save the node.

When a thermometer node is displayed, the thermometer should be rendered directly underneath the title.

Optionally, use a module like Nodeblocks to convert thermometers into blocks.  From there, you can place thermometers anywhere on the site using any number of mechanims.

Current Limitations
=====================================================
* Currently, this module does not allow for the thermometer colour to be changed from the red gradient.  This is a high-priority item and will be covered in a subsequent release.
